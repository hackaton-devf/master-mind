import http.client


token1 = 'eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsTWdoNnViZlFzU0Fldl9GTlczZ3N3In0.JpunG1WmI3Q'

token2 = 'RXOWowUzgtaEQzMy13YXlpNkEiLCJleHAiOjE1OTIxNTU2NjgsImlhdCI6MTU5MTU1MDg2OH0.0o477UkJZcZtVQ4g_dtPPDssFp960cod9t174GHYTlw'

zoom_url = 'https://zoom.us/oauth/authorize?response_type=code&client_id=YJIn46WeTfyEljg&redirect_uri=https%3A%2F%2Fsage-wise.net%2F'


class ZoomApi(object):
	"""
	"""
	@classmethod
	def connection(self):
		conn = http.client.HTTPSConnection("api.zoom.us")
		headers = {
			'authorization': "Bearer "+token2,
			'content-type': "application/json"
			}
		return conn, headers

	@classmethod
	def decode(self, conn):
		import json

		res = conn.getresponse()
		data = res.read()
		# dataD = data.decode("utf-8")
		# print(dataD)
		return json.loads(data)
