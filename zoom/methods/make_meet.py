from zoom.zoom_api import ZoomApi
import json
from datetime import datetime
# Instant, Schedule, Recurring no ficed time, rec fixed time


def make_meet():
	now = datetime.now()
	body = {
	"topic": "Master Mind Hackaton",
	"type": "1", 
	"start_time": now.strftime("%d/%m/%Y %H:%M:%S"),
	"duration": "20",
	"password": "MasterMind",
	"settings": {
		"host_video": "true",
		"participant_video": "true",
		"cn_meeting": "true",
		"in_meeting": "false",
		"join_before_host": "true",
		"mute_upon_entry": "true",
		"watermark": "false",
		"use_pmi": "true",
		"approval_type": "0",
		"audio": "both",
		"auto_recording": "none",
		"enforce_login": "false",
		}
	}

	conn, headers = ZoomApi.connection()
	conn.request("POST", "/v2/users//meetings", headers=headers, body=json.dumps( body ))
	request = ZoomApi.decode(conn)
	print(request)
	return request
