from django.views.generic import View
from django.http import HttpResponse

# -*- coding: utf-8 -*-
# from __future__ import unicode_literals

from django.shortcuts import render
from whatsapp.models import Message
from whatsapp.handlers.master_mind import MasterMindHandleMessage
# Create your views here.




class BotWebhook(View):
	"""Handles incomming messages to Webhook from Telegram."""

	def post(self, request, *args, **kwargs):
		try:
			w_message = request.POST
			# Save incomming message
			message, created = Message.objects.get_or_create(
				id=w_message.get('MessageSid'),
				defaults={
					'sender_id': w_message.get('From'),
					'message': w_message.get('Body', ''),
					'receptor_id': w_message.get('To'),
					'media': w_message.get('MediaUrl0', ''),
				}
			)

			handler = MasterMindHandleMessage(message, from_=w_message.get('To'))
			handler.handle_it()
		except Exception as e:
			# Only for debugging...
			raise e

		# Everything it's ok
		return HttpResponse()

