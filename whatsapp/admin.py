from django.contrib import admin

from . import models

class MessageAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'sender_id',
        'message',
        'canceled',
        'current_step',
        'completed',
        'command',
        'created',
]

class MeetAdmin(admin.ModelAdmin):
    list_display = [
        'status',
        'user',
        'created',
    ]



 
admin.site.register(models.Message, MessageAdmin)
admin.site.register(models.Meet, MeetAdmin)
