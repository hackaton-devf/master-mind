from django.db.models import Q

from whatsapp.commands.master_mind_register import MasterMindRegister
from whatsapp.commands.master_mind_meet import MasterMindMeet
from annoying.functions import get_object_or_None
from helpers.whatsapp_bot import send_message, get_client
from helpers import general_copies
from whatsapp.models import Message
from time import sleep
from helpers.numbers import numbers_to_emojis
import re
import os
import random


def cancel_last_operation(bot, message, **kwargs):
    """Cancel last operation

    Args:
        bot: PTB Instance of Bot class.

        message: telegram_bot.Message model instance

    Kwargs:
        last_message: Previous telegram_bot.Message instance.
    """
    message.completed = True
    # message.save()
    MasterMindHandleMessage.message = message
    MasterMindHandleMessage._send_i_dont_understand_message(message)
    message.save()

def send_help_message(bot, message, *args, **kwargs):
    message.completed = True
    message.save()
    help_text = general_copies.help_message(
        commands=MasterMindHandleMessage.available_commands_as_str_list())
    send_message(
        to=message.sender_id,
        from_=kwargs.get('from_'),
        body=help_text,
        # reply_markup=telegram.ReplyKeyboardHide(hide_keyboard=True)
        )

def in_progress(bot, message, *args, **kwargs):
    message.completed = True
    message.save()
    send_message(
        to=message.sender_id,
        from_=kwargs.get('from_'),
        body="""Este módulo se encuentra en desarollo y estará disponible muy pronto 😊""",
        )


def reset_user(bot, message, *args, **kwargs):
    return True


class UnknownCommand(Exception):
    pass


class MasterMindHandleMessage(object):
    """Parse message received through webhook.

    If it's a command delegates message to corresponding action.

    If it's not, we check the last message from this user to see
    if there is something left to do about that message.

    Finally, if we don't know what to do, we send a help text
    with available commands, except the basics /start and /stop

    Args:
        message:
            An instance of Message Model of whatsapp_bot app
    """

    available_commands = {
        'inicio': {
            'description': 'cancelar la operación actual',
            'action': cancel_last_operation,
            'list_position': -1,
        },
        'registro': {
            'description': 'registrar tu whatsapp a tu cuenta',
            'action': MasterMindRegister.start,
            'subcommands': ['siguiente', 'finalizar'],
            'list_position': 1
        },
        'meet': {
            'description': 'registrar guardia',
            'action': MasterMindMeet.start,
            'subcommands': ['siguiente', 'finalizar'],
            'list_position': 1
        },
        'reset': {
            'description': 'resetear tu usuario en el chatbot.',
            'action': reset_user,
            'list_position': -1
        },
    }

    # Ignored commands for help text
    # secret_commands = ['start', 'stop', 'ayuda', 'cancelar', 'resumen']
    secret_commands = ['inicio', 'reset', 'registro', 'meet']
    initial_commands = ['']
    all_commands = ['registro', 'inicio', 'reset', 'meet']
    # Commands that does not require authentication
    no_auth_commands = ['registro', 'inicio',]

    def __init__(self, message, *args, **kwargs):
        from whatsapp.models import Message
        self.message = message
        self.from_ = kwargs.get('from_')
        self.user_id = self.message.sender_id
        self.last_message = Message.objects.filter(
            ~Q(id=self.message.id),
            sender_id=self.message.sender_id
        ).first()

        if (self.last_message and
           (self.last_message.canceled is True or
               self.last_message.completed is True)):
            self.last_message = None

        self.bot = get_client()

    @classmethod
    def available_commands_as_choices(cls):
        """Create a tuple with available commands.

            Returns:
                Tuple with available commands.
        """
        return tuple([
            (key.lower(), key.lower(),)
            for key in cls.available_commands.keys()
        ])

    def _auth_people_user(self):
        """
        Validates if there is one nurse with the Telegram ID of the sender.
        """
        # print('customer', '_auth_people_user')
        # customer = get_object_or_None(CustomersUser, whatsapp_id=self.user_id, client__id=43)
        # if not pos and not edecan and not promoter and not coordinator:
        # if not customer:
            # self.message.canceled = True
            # self.message.save()
            # send_message(
            #     to=self.user_id,
            #     from_=self.from_,
            #     body=general_copies.registry_init
            # )
            # return False
        return True

    @classmethod
    def all_available_commands_as_str_list(cls):
        """Creates a readable representantion of available commands.

        Returns: String with all commands available, for example::

            1. **/llegada** - Avisar que has llegado a un servicio.
            2. **/salida** - Avisar que has finalizado el servicio.
        """
        commands_list = []
        sorted_command_keys = sorted(
            cls.available_commands,
            key=lambda k: cls.available_commands[k]['list_position'])
        for cmd in sorted_command_keys:
            data = cls.available_commands.get(cmd)
            if cmd in cls.secret_commands:
                continue

            total_commands = len(commands_list)
            try:
                cmd = numbers_to_emojis[cmd] if numbers_to_emojis[cmd] else cmd
            except:
                pass
            commands_list.append(
                '{name} {description}'.format(
                    num=total_commands + 1,
                    name=cmd,
                    description=data.get('description', '')
                )
            )
        return '\n'.join(commands_list)

    @classmethod
    def available_commands_as_str_list(cls):
        """Creates a readable representantion of available commands.

        Returns: String with all commands available, for example::

            1. **/llegada** - Avisar que has llegado a un servicio.
            2. **/salida** - Avisar que has finalizado el servicio.
        """
        commands_list = []
        sorted_command_keys = sorted(
            cls.available_commands,
            key=lambda k: cls.available_commands[k]['list_position'])
        for cmd in sorted_command_keys:
            data = cls.available_commands.get(cmd)
            if cmd not in cls.initial_commands:
                continue

            total_commands = len(commands_list)

            try:
                cmd = numbers_to_emojis[cmd] if numbers_to_emojis[cmd] else cmd
            except:
                pass

            commands_list.append(
                '{name} {description}'.format(
                    num=total_commands + 1,
                    name=cmd,
                    description=data.get('description', '')
                )
            )
        return '\n'.join(commands_list)

    @classmethod
    def _send_i_dont_understand_message(self, message):
        """Send Telegram Message with some helpful commands."""
        self.message.completed = True
        self.message.save()

        from users.models.users import MasterMindUser
        user = MasterMindUser.objects.filter(whatsapp_id=message.sender_id).first()

        welcom_message = 'Hola!'
        continue_message = '*registro*'
        if user:
            welcom_message = """ Hola {}. Bienvenido/a de regreso!""".format(user.name)
            continue_message = '*meet*'

        send_message(
            to=message.sender_id,
            from_=message.receptor_id,
            body="""{}, soy el *Bot de Master Mind* y te conectaré a workshops de expertos para que con un brain-storm se resuelvan tus  inquietudes de negocios, tecnologías o apoyo técnico, sólo regístarte y solicita un meet, gracias por ser parte de *Master Mind Bot*!

Escribe {} para continuar""".format(welcom_message, continue_message),
)


    def _exec_command(self, cmd, step=1):
        """Check if we can handle this command, if so execute a function."""
        # Set this message command
        available_subcommands = {}
        available_commands = self.available_commands
        subcommand = ''
        if self.available_commands.get(cmd, None):
            self.message.command = cmd
            # self.message.subcommand = cmd

        elif self.last_message:
            self.message.current_step = self.last_message.current_step
            previous_command = self.available_commands.get(self.last_message.command, None)

            if previous_command:
                available_subcommands = previous_command.get('subcommands', {})
                self.message.command = self.last_message.command

        # Yeah, we know what you are trying to do... sort of.

        self.message.save()
        # if self.message.command not in self.no_auth_commands:
        #     # If not in the VIP "list", we need to know how are you.
        #     if not self._auth_people_user():
        #         return

        # Okey, you are VIP, lets move on.
        cmd_name = self.message.command
        cmd = self.available_commands.get(cmd_name, {})
        to_exec = cmd.get('action')
        if not to_exec:
            MasterMindHandleMessage.message = self.message
            self._send_i_dont_understand_message(self.message)
        # Yep, we can help you :)
        to_exec(
            bot=self.bot, message=self.message,
            step=step,
            last_message=self.last_message,
            # last_attempt=last_attempt,
            subcommand=subcommand,
            from_=self.from_,
        )

    def _parse_message(self):
        commands = True
        try:
            if self.last_message.command == 'consultant':
                print('self.last_message.commandself.last_message.command X', self.last_message.command)
                commands = False
        except Exception as e:
            commands = True

        print('self.last_message.command', commands)
        MasterMindHandleMessage.message = self.message
        if commands and self.message.message.lower() in self.secret_commands:
            print('command: ', self.message.message.lower())
            self._exec_command(self.message.message.lower())
        elif self.last_message:
            print('aqui debería', self.last_message.current_step)
            self._exec_command(
                self.last_message.command,
                step=self.last_message.current_step + 1)
        elif self.message.message.lower() in self.all_commands:
            print('se va hasta acá')
            self._exec_command(self.message.message.lower())
        elif not self.last_message:
            self._send_i_dont_understand_message(self.message)
        else:
            print('ñultkmo')
            self._exec_command(
                self.last_message.command,
                step=self.last_message.current_step + 1)

    def handle_it(self):
        self._parse_message()
