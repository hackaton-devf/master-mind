
def send_messages(message, copies = {'patient':[], 'specialist':[], 'super_Liora':[]}):
	from helpers.whatsapp_bot import send_message

	meet = copies.get('meet', None)

	if meet:
		for _m in meet:
			try:
				sleep = _m.get('s', None)
				if sleep:
					sleep(sleep)
			except:
				pass

			try:
				mensaje = _m.get('m', None)
				if mensaje:
					send_message(
						to=message.sender_id,
						from_=message.receptor_id,
						body=mensaje
						)
			except:
				pass
