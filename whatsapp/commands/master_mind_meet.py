from django.core.files.base import ContentFile

from whatsapp.functions.master_mind import send_messages
from helpers.whatsapp_bot import send_message
from whatsapp.copies.master_mind import (COPIE_OPTION_NO_VALID,
COPIE_MASTERMIND_MEET,
COPIE_MASTERMIND_MEET_WAIT,
COPIE_MASTERMIND_NEED_REGISTER,
)

import json


class MasterMindMeet(object):
    """Registry-related ."""

    @classmethod
    def stop(cls, bot, message, **kwargs):
        """Handles **stop** command steps.

        Set whatsapp_id of the people sending the message, to empty string.

        Args:
            bot: A valid instance of PythonTelegramBot Bot Class

            message: A valid instance of telegram_bot.Message model.
        """

        if message.people:
            send_message(
                to=message.sender_id,
                from_=message.receptor_id,
                body='Adiós :)')
            message.people.whatsapp_id = ''
            message.completed = True
            message.people.save()

    @classmethod
    def start(cls, bot, message, step=1, **kwargs):
        try:
            response = int(message.message)
        except Exception as e:
            response = message.message

        try:
            extra_data_dict = json.loads(kwargs.get('last_message').extra_data)
            message.extra_data = json.dumps(extra_data_dict)
        except Exception as e:
            message.extra_data = json.dumps({})
        print(step)

        message.current_step = step

        COPIES = {
            'meet': [],
        }


        from users.models.users import MasterMindUser
        user = MasterMindUser.objects.filter(whatsapp_id=message.sender_id).first()


        if step == 1:
            if not user:
                COPIES['meet'].append({'m': COPIE_MASTERMIND_NEED_REGISTER})
                message.completed = True
            else:
                COPIES['meet'].append({'m': COPIE_MASTERMIND_MEET.format(user.name)})

        if step == 2:
            try:
                response = message.message.lower()
            except:
                pass

            if response in ['si', 'sí', '*si*', '*sí*']:
                from whatsapp.models import Meet
                meet =  Meet.objects.create(user=user)

                if meet:
                    COPIES['meet'].append({'m': COPIE_MASTERMIND_MEET_WAIT})
                    message.completed = True
                else:
                    print('NO MEET')
            else:
                message.completed = True


        # if step == 3:
                
        #     COPIES['meet'].append({'m': COPIE_MASTERMIND_AREA})

        # if step == 4:
        #     if response == 'terminar':
        #         print('terminar')
        #     COPIES['meet'].append({'m': COPIE_MASTERMIND_SPECIALITY})
            

        # if step == 5:
        #     #     COPIES['meet'].append({'m': COPIE_MASTERMIND_REGISTER_THANKS})
        #     # else:
        #     COPIES['meet'].append({'m': COPIE_MASTERMIND_AREA_finish})

        #     message.current_step = step - 2




        # if step == 2:
        #     print('response', response)
        #     if response in [1, 2]:

        #         from cadena.models.cadena_specialist import CadenaSpecialist
        #         # obtener al usuario por su whats
        #         cadena_specialist = CadenaSpecialist.objects.filter(whatsapp_id=message.sender_id).first()
        #         message.completed = True

        #         # validar si existe o no
        #         if cadena_specialist:
        #             # validar si tiene status 1,2,3
        #             print('cadena_specialist.validated', cadena_specialist.validated)
        #             if cadena_specialist.validated in [1,0]: # Pendiente
        #                 COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_PENDING)
        #             elif cadena_specialist.validated == 2: # Validado
        #                 extra_data_dict['to_do'] = response
        #                 message.extra_data = json.dumps(extra_data_dict)
        #                 if response == 1:
        #                     COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_START)
        #                     # STATUS A SCTIVO
        #                     cadena_specialist.active = True
        #                     cadena_specialist.available = True
        #                     cadena_specialist.save()

        #                 elif response == 2:
        #                     if not cadena_specialist.available and cadena_specialist.active: # NO Disponible
        #                         COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_UNAVAILABLE)
        #                         message.completed = False
        #                     else:
        #                         COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_END)
        #                         cadena_specialist.active = False
        #                         cadena_specialist.available = False
        #                         cadena_specialist.save()

        #             elif cadena_specialist.validated == 3: # Rechazado
        #                 COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_REJECTED)
        #             else:
        #                 COPIES['patient'].append("Hubo un error con tu usuario, solicita ayuda a soporte *Kol Cadena*")
        #         else:
        #             COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_NOT_FOUNDED)
        #     else:
        #         COPIES['patient'].append(COPIE_OPTION_NO_VALID)
        #         message.current_step = step -1
        #     print(COPIES)

        # if step == 3:
        #     if type(response) == type('') and response.replace(' ', '').replace('*', '').lower() == 'entendido':
        #         from cadena.models.cadena_specialist import CadenaSpecialist
        #         # obtener al usuario por su whats
        #         cadena_specialist = CadenaSpecialist.objects.filter(whatsapp_id=message.sender_id).first()

        #         COPIES['patient'].append(COPIE_MASTERMIND_WORKSHIFT_END)
        #         COPIES['super_Liora'].append(COPIE_MASTERMIND_WORKSHIFT_END_SuperLiora.format(cadena_specialist.name))

        #         cadena_specialist.active = False
        #         cadena_specialist.available = False
        #         cadena_specialist.save()

        #         message.completed = True
        #     else:
        #         COPIES['patient'].append(COPIE_OPTION_NO_VALID)
        #         message.current_step = step -1

        # ###############################################
        # ###############################################

        send_messages(message, COPIES)
        message.save()
