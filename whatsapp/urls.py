from django.conf.urls import url

from whatsapp.views import ( BotWebhook )
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^master-mind/$', csrf_exempt(BotWebhook.as_view()), name='handler'),
]
