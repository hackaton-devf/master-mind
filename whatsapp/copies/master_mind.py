
COPIE_OPTION_NO_VALID = """La opción seleccionada no es válida"""



COPIE_MASTERMIND_WORKSHIFT_BEGIN = """Hola!, soy el *Bot de Master Mind*  🤖 y te conectaré a workshops de expertos para que con un brain-storm se resuelvan tus  inquietudes de negocios, tecnologías o apoyo técnico, sólo regístrate y solicita una reunión, gracias por ser parte de *Master Mind Bot!*

Si necesitas más información visita https://mastermindbot.com

Escribe *registro* o *meet* para continuar
"""

COPIE_MASTERMIND_REGISTER_NAME = """¿Cuál es tu nombre completo?"""

COPIE_MASTERMIND_REGISTER_SECTOR = """Elige la opción que mejor te representa


1.- Emprendedor
2.- Empleado
3.- Profesionista
4.- Freelance
5.- Estudiante

"""

COPIE_MASTERMIND_REGISTER_AREA = """por favor, escribe un sector o área en el que te especialises o sea de tu interés"""

COPIE_MASTERMIND_REGISTER_AREA_finish = """por favor, escribe un sector o área en el que te especialices o sea de tu interés

Escribe *inicio* cuando concluyas el registro.
"""


COPIE_MASTERMIND_REGISTER_SPECIALITY = """En que nivel manejas esa área o sector:

1 Avanzado
2 Intermedio
3 Básico
4 Áreas de interés
"""


COPIE_MASTERMIND_REGISTER_REGISTER_THANKS = """Muchas gracias por llenar este breve registro, cuando un microempresario agende una master mind contigo, recibirás un whatsapp con la solicitud, si deseas aceptar escribe si y serás incluido en la reunión, al final recibirás un mensaje con la liga para acceder vía zoom."""



COPIE_MASTERMIND_MEET = """{}, ¿deseas solicitar un meet para hacer un workshop con otros usuarios?"""


COPIE_MASTERMIND_MEET_WAIT = """En este momento estoy buscando otras personas que estén disponibles para asistir a tu Sesión Master Mind. Dame un par de minutos y te avisaré cuando esté lista.

Mientras tanto, te comento las reglas de la dinámica:
- El Master Mind Bot creará una reunión con 4 a 6 personas de perfiles similares.
- Una vez iniciada la sesión, te debes presentar con nombre, profesión y giro. 
- El éxito de la Sesión Master Mind es que todos los integrantes participen en algún momento. No se permiten oyentes. 
- Cada participante tendrá un máximo de 5 minutos para presentar su situación. Después, los demás integrantes se tomarán de 5 a 10 minutos para tratar su tema. 
- Una vez que todos hayan participado, se procede a la convivencia libre.
"""


COPIE_MASTERMIND_REGISTER_THANKS = """{}, gracias por haberte registrado. Seguramente de que encontrarás la ayuda que necesitas para reinventar tu modelo de negocio. Juntos venceremos esta crisis!

Escribe *inicio* o *meet* para continuar
"""


COPIE_MASTERMIND_NEED_REGISTER = """Necesitas estar registrado

Escribe *registro* o *meet* para continuar
"""

