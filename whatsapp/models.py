from django.db import models

from base.models import BaseModel
from annoying.functions import get_object_or_None


class Message(BaseModel):
    id = models.CharField(max_length=100, primary_key=True, unique=True, editable=False, blank=True)
    sender_id = models.CharField(max_length=100)
    receptor_id = models.CharField(max_length=100)
    message = models.TextField()
    canceled = models.BooleanField(default=False)
    current_step = models.PositiveIntegerField(default=0)
    completed = models.BooleanField(default=False)
    command = models.CharField(max_length=50, null=False, default='')
    subcommand = models.CharField(max_length=50, null=False, default='')
    extra_data = models.TextField(default='', blank=True)
    media = models.URLField(max_length=300, default='', blank=True)
    location = models.CharField(max_length=300, default='', blank=True)


    class Meta:
        ordering = ['-created']  # LIFO










STATUS_CHOICES = (
    (0, 'Creado'),
    (1, 'En Proceso'),
    (2, 'Asignado'),
)


class Meet(BaseModel):
    status = models.PositiveIntegerField(default=0, choices=STATUS_CHOICES, blank=True)
    user = models.ForeignKey('users.MasterMindUser', on_delete=models.CASCADE)


    class Meta:
        ordering = ['created']  # LIFO

    def save(self, *args, **kwargs):
        print('SAVE')
        if self.status == 0:
            meets = Meet.objects.filter(status=0).select_related('user')
            print(len(meets))

            arr_users = []

            if len(meets) > 4:
                cent = 0
                from helpers.whatsapp_bot import send_message
                from zoom.methods.make_meet import make_meet
                new_meet = make_meet()

                for meet in meets:
                    if cent < 5:

                        cent+=1
                        meet.status = 1
                        meet.save()
                        arr_users.append(meet.user)

                        if cent ==4:

                            for u in arr_users:
                                send_message(
                                    to=u.whatsapp_id,
                                    from_='whatsapp:+14155238886',
                                    body="""
    Master Mind Bot te hace la mas cordial invitación al meet para hacer workshop de temas muy interesantes, te esperamos:!

    {}
    """.format(new_meet.get('join_url'))
                            )

                    else:
                        return super().save(*args, **kwargs)


        return super().save(*args, **kwargs)
