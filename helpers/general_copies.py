### DELETE THIS FILE ONCE TELEGRAM_BOT IS REMOVED

"""This file contains copies for commands."""
import os





def help_message(commands):
    return """¿Qué quieres hacer? Puedes excribir alguna de éstas opciones:

{}
""".format(commands)


user_not_assigned = "Aún no hay un usuario ligado a esta cuenta de whatsapp, para registrar escribe *registro*"




# bot_error = 'No he podido entender lo que me pides.\
# Vuelve a intentar o ponte en contacto con nosotros.'

# start_request_email = """
# Por favor escribe tu teléfono a 10 dígitos"""

# start_request_first_name = """
#         Perfecto, ahora escribe sólo tu *nombre* o nombres. Tus apellidos los solicitaremos en la siguiente pregunta."""

# def start_request_last_name(name):
#     return """Gracias {}, ya casi!, ayúdanos escribiendo tus *apellidos* para finalizar tu registro.""".format(name)

# start_email_not_found = """
# No tenemos ninguna demo con este número, \
# verifica que sea correcto y vuelve a intentarlo.
# Si no recuerdas cúal es consulta con el equipo de Redmkti.
# """

# start_already_linked = """Este usuario ya tiene asociado esta cuenta de whatsapp. Porfavor contacta
# al equipo de Redmkti.
# """
# email_already_registered = """Este correo ya esta registrado en la plataforma, favor de verificarlo e intentar de nuevo.
# """


# def start_ok(nurse_name, commands):
#     return """¡Listo {}! Ahora si, podemos empezar. Para comenzar escribe alguno de estos comandos:
# {}
# """.format(nurse_name, commands)

# def checkin_in_activation(people_name, name):
#     return """{}, ¿Estás haciendo tu checkin para *{}*?
#     *Si*
#     *No*
#     """.format(people_name, name)

# def checkout_activation(people_name, name):
#     return """{}, estas a punto de marcar tu salida de {}, es correcto?
#     *Si*
#     *Cancelar*
#     """.format(people_name, name)


# def checkout_event(edecan_name, name, city, state):
#     return """{}, estas a punto de marcar tu salida de {} en {} {}, es correcto?
#     *Si*
#     *Cancelar*
#     """.format(edecan_name, name, city, state)

# def checkin_completed(people_name, activation_name, date):
#     return """
#     {} tu entrada a {} ha sido registrada con éxito a las {}
#     Para completar tu guía escribe *guia*
#     """.format(people_name, activation_name, date)

# def guide_completed(name):
#     return """
#     {} tu reporte de ha sido _completado con éxito_. Si deseas agregar otro reporte escribe *inicio*.

# Si quieres el cupon del dia de hoy escribe la palabra *cupon* ✌. Su canje termina hoy a las 10 de la noche. 🎯
# """.format(name)

# def checkout_guide_completed(people_name):
#     return """
#     {} tu guía de checkout ha sido completada con éxito. Muchas gracias y que tengas muy buen día!
#     """.format(people_name)

# def checkout_completed(people_name, activation_name, date):
#     return """
#     {} tu salida a {} ha sido registrada con éxito a las {}. Para completar tu guía de salida escribe *guia*.
#     """.format(people_name, activation_name, date)

# def start_already_registered_email(*args):
#     return """Hola {}! Ya estas registrado(a) en la plataforma. Escribe *ayuda* para consultar los comandos disponibles para interactuar conmigo.
# Recuerda que soy un robot y solamente respondo a comandos específicos.
# """.format(*args)

# # ============ #
# #   Checkin    #
# # ============ #

# checkin_not_available_shifts = """No tienes activaciones disponibles para marcar tu entrada. \
# Intenta de nuevo en unos minutos."""

# checkout_not_available_shifts = """No tienes activaciones disponibles para marcar tu salida. \
# Marca primero tu entrada escribiendo *entrada* o si ya lo hiciste consulta con el equipo de Red."""

# guide_not_available = """No tienes activaciones disponibles para marcar tu guía. \
# Marca primero tu entrada escribiendo *entrada* o si ya lo hiciste consulta con el equipo de Red."""


# checkin_invalid_location = """
# La ubicación enviada no es la correcta, necesitas estar \
# en el domicilio de la activación.
# Si hubo algún cambio o necesitas avisarnos sobre alguna \
# eventualidad, contacta al equipo de RedConnect -

# Si no hay ningún error entonces vuelve a intentarlo y \
# asegúrate que el pin esté en la ubicación correcta."""


# def checkin_already_checked_in(name):
#     return """Ya has registrado tu entrada para {}, \
# Escribe *guia* para completar tu guía o si ya la realizaste escribe *salida*""".format(name)

# # ============ #
# #   Checkout   #
# # ============ #

# checkout_no_checkin = 'No has iniciado algún servicio reciéntemente'

# checkout_confirm_shift = 'Confirma el turno en el que deseas marcar tu salida.'

# checkout_invalid_shift = """Lo siento, no he encontrado el turno seleccionado.
# Selecciona un turno válido."""

# checkout_ok = """Listo, muchas gracias.
# Repite este proceso en tu siguiente servicio.

# Recuerda que cualquier cosa que necesites puedes reportarla hablando \
# con nuestro equipo de humanos dando click \
# aquí - @pazmental - """


# def checkout_already_checked_out(*args):
#     return """Este servicio fue terminado a las {}, \
# si es un error, por favor escríbenos dando clic aquí - @pazmental -.""".format(
#         *args)


# # ============ #
# #   Registry   #
# # ============ #

# registry_init = 'Necesitas registrarte para empezar. Escribe *registro* para iniciar.'

# # ============ #
# #     Help     #
# # ============ #

# help_unavailable_command = 'Disculpa, por el momento no puedo realizar esta acción'


# help_no_action = 'No has realizado alguna acción ultimamente.'













# def help_canceled_command(command):
#     return 'Ok, he dejado de ejecutar el comando *{cmd}*'.format(
#         cmd=command)


# def hr_help_list_commands(commands):
#     return """
# Hola, ¿cómo estás? Te saluda tu HRBP Virtual de Jabil Green Point, a partir de ahora podremos estar en contacto, mandarte avisos, invitaciones y encuestas a través de este número
# Para empezar escribe alguno de estos comandos:

# {commands}

# Recuerda que soy un robot. No puedo entender, escríbeme las instrucciones mencionadas de forma clara.
# """.format(commands=commands)

# def help_list_commands(commands):
#     return """
# Hola soy Promo Bot, tu robot personal. Para empezar escribe alguno de estos comandos:

# {commands}

# Recuerda que soy un robot. No puedo entender, escríbeme las instrucciones mencionadas de forma clara.
# """.format(commands=commands)

# error = 'Ocurrió un error, por favor comunícate con nosotros.'

# user_types_options = """
# Excelente, ahora porfavor *escribe el número* que corresponda
# a tu tipo de usuario

# *1* para Cliente ó Empleado
# *2* para Asesor
# *3* para Gerente de zona
# """

# # Staff Notifications
# def nurse_too_many_command_attempts(attempt):
#     return """Oye, {} ha intentado ejecutar el comando - {} - {} veces, \
# las razones de fallo son:

# {}
# """.format(
#         attempt.nurse.full_name, attempt.command,
#         attempt.failing_count, attempt.reasons)


# def checkin_in_request_project(name):
#     return """{}, porfavor escribe el número de proyecto. Si no lo tienes a la mano consulta con el equipo de Redmkti.
#     """.format(name)

# def checkin_in_request_event(name):
#     return """{}, porfavor escribe el *folio del evento* al que quieres marcar entrada. Si no lo tienes a la mano consulta con el equipo de Redmkti.
#     """.format(name)

# def checkin_in_request_business_name(project_name):
#     return 'Muy bien, indicaste el proyecto {}, ayúdame escribiendo el *nombre la tienda* a la que vas a activar'.format(project_name)

# def checkin_in_request_event_city(event_name):
#     return 'Muy bien, indicaste el evento {}, ayúdame escribiendo la *cuidad* donde se realiza el evento'.format(event_name)

# def  request_event_state(locations):
#     return """Gracias, ahora porfavor escribe el *numero* del *estado* en las siguientes opciones el que se realiza el evento:\

#     {}
#     """.format(locations)

# def checkin_in_request_state(locations):
#     return """Gracias, ahora porfavor escribe el *numero* del *estado* en las siguientes opciones el que se realiza la activación:\

#     {}
#     """.format(locations)

# request_city = "Porfavor escribre la *cuidad* en la que estas haciendo tu ativación."

# def checkin_event_completed(name, event, city, state, checkin):
#     return """{}, tu entrada al evento {} en {} {} ha sido registrada con éxito a las {}

#     Escribe *guía* para comenzar con tu guía de activación.
#     """.format(name, event, city, state, checkin)

# def checkin_in_not_general_project(number):
#     return '''Ups, el número de proyecto {} que escribiste *no existe*. Porfavor verifícalo e intenta de nuevo.
#     Si no lo tienes a la mano consulta con el equipo de Redmkti.
#     '''.format(number)

# def checkin_in_not_event(number):
#     return '''Ups, el folio {} que escribiste *no existe*. Porfavor verifícalo e intenta de nuevo.
#     Si no lo tienes a la mano consulta con el equipo de Redmkti.
#     '''.format(number)

# def checkin_in_request_chain(name):
#     return '''Excelente, ahora porfavor indica la *cadena* de la tienda {}
#     '''.format(name)

# not_tasks_found = 'Al parecer no hay instrucciones definidas para este cliente. Intenta de nuevo mas tarde. Gracias!'

# def task_help_text(task):
#     text = ""
#     # try:
#     #     if task.belongs_to == 2 or task.needs_evidence:
#     #         text = ''
#     #     else:
#     #         text = ' Escribe *si* o *no* para continuar.'
#     # except Exception as e:
#     #     print(e)
#     #     text = ""
#     return text

# def asset_task_help_text(asset, task):
#     return """Excelente, estas a punto de reportar el activo {}, Si no es el activo deseado puedes escribir *cancelar* e intentar de nuevo, de lo contrario ayúdanos contestando las siguientes instrucciones:
# {}
# """.format(asset, task)


# def checkin_in_request_location(name):
#     return """
#     {} antes de comenzar, porfavor envía una imagen de tu ubicación actual.
#     """

# def request_alarm_type(name, no_understand=False):
#     return """{} {}, escribe el tipo de alarma que deseas denunciar:

# *1* para Producto
# *2* para Anomalía
# *3* para Acoso

# """.format(name, 'no pude entenderte' if no_understand else '')

# def request_alarm_message(name):
#     return """Porfavor escribe información sobre la alarma que reportas.""".format(name)

# def alarm_completed(name):
#     return """{}, Tu denuncia ha sido reportada con éxito.""".format(name)

# def request_location(message, user_type):
#     return """Para concluir tu entrada, asegúrate de *leer y marcar de enterado el flyer* en la siguiente liga:

# {}/api/message-locations/{}/{}
# """.format(os.getenv('REDCONNECT_SITE_URL'),user_type,message)

# def error_request_location(message, user_type):
#     return """Aún no has confirmado tu consulta del flyer.
# Para concluir tu entrada, asegúrate de *leer y marcar de enterado el flyer* en la siguiente liga:

# {}/{}-flyer/{}
# """.format(os.getenv('REDCONNECT_CLIENT_SITE_URL'),user_type,message)

# def success_reset_user(name):
#     return "Listo, el usuario {} ha sido reseteado con éxito, para registrar de nuevo el usuario escribe *registro*".format(name)



# def welcome_guide(name, instruction):
#     return """Hola {}, estas a punto de iniciar tu reporte.

# {}
# """.format(name, instruction)

# def welcome_hr_guide(name, instruction):
#     return """Hola {}, te pedimos nos ayudes a contestar estas preguntas.

# {}
# """.format(name, instruction)


# request_pos_asset_name = "Gracias, ahora porfavor indica el _nombre_ del activo"

# request_pos_type = """Muy bien! a continuación _escribe el número_ que corresponda a la categoría del activo:

# *1* para Refrigerador
# *2* para Promocional
# *3* Otro
# """

# def require_pos_client(choices):
#     return """Para comenzar tu registro, porfavor _escribe el número_ de la empresa__ a reportar:
# {}
# """.format(choices)

# def require_pos(choices):
#     return """Perfecto ahora porfavor, _escribe el número de la tienda__ a la que perteneces:
# {}
# """.format(choices)

# request_user_email="Gracias! favor de _escribir tu correo electrónico_"

# request_pos_name = "Excelente, para concluir tu registro porfavor *escribe tu nombre*"

# def pos_registry_completed(name):
#     return """{} tu registro ha sido completado con éxito. Ahora puedes registrar tu guía de ejecución de tus activos escribiendo *guía*""".format(name)

# def client_id_not_found(id, options):
#     return """El número {} que escribiste no es válido favor de verificarlo e intentar de nuevo con alguna de estas opciones:
# {}
# """.format(id, options)

# def customer_registry_completed(name):
#     return """{} tu registro ha sido completado con éxito. Ahora puedes continuar escribiendo *inicio*""".format(name)

# cutomer_request_name = "Perfecto, ahora escribe sólo tu *nombre* o nombres. Tus apellidos los solicitaremos en la siguiente pregunta."

# def customer_request_last_name(name):
#     return """Gracias {}, ayúdanos ahora escribiendo tus *apellidos*.""".format(name)

# def email_not_valid(name, email):
#     return "{}, el correo {} que escribiste *no es válido* porfavor verídicalo e intenta de nuevo.".format(name, email)

# def welcome_customer_guide(instruction):
#     return """Hola, déjame presentarme. Soy un robot de Recursos Humanos capaz de transmtir tus necesidades al comité de la empresa.
# No estoy autorizado para entablar conversaciones fuera de lo que te voy a preguntar.
# Te aseguro que tus respuestas serán tratadas con el máximo cuidado y respeto.

# {}
# """.format(instruction)


# def request_store_name(name):
#     return """Muchas gracias {}, ahora necesitaremos un poco de información sobre el punto de venta.
# ¿Cuál es el nombre de la tienda?""".format(name)


# def request_pos_state(locations):
#     return """Excelente, ahora porfavor escribe el *numero* del *estado* en las siguientes opciones al que pertenece la tienda:\

#     {}
#     """.format(locations)

# def request_trade_state(locations):
#     return """Excelente, ahora porfavor escribe el *numero* del *estado* en en el que realizas tu operación:\

#     {}
#     """.format(locations)

# request_pos_city = "¿En que ciudad se encuentra la tienda?"

# request_trade_city = "Gracias, ¿En que ciudad realizas tu operación?"

# pos_registry_completed = "Felicidades! Tu registro y el registro de la tienda han sido completados con éxito. Ahora puedes continuar escribiendo *inicio*"


# def redeem_coupon(name, coupon, url):
#     return """Felicidades {}, en la siguiente liga podrás encontrar tu cupón: {}

# {}

# Que tengas excelente día!
# """.format(name, coupon, url)

# not_contracts = "No hay contratos disponibles para este usuario."

# def contract_response(name, url):
#     return """{}, al dar click en la siguiente liga podrás consultar tu contrato:

# {}
# """.format(name, url)

# def goto_ecommerce(name, url):
#     return """{}, en la siguiente liga podrás consultar los productos disponibles

# {}
# """.format(name, url)

# def coupon_welcome(name):
#     return """Hola {}, para redimir la promoción porfavor escribe el nombre del cupón.""".format(name)

# def redeem_coupon_miss(coupon_name):
#     return """Lo sentimos, el cupón {} no es válido intenta de nuevo o escribe *cancelar* para intentar en otro momento.""".format(coupon_name)

# def redeem_coupon_completed(name, coupon, url):
#     return """Felicidades {}, has redimido exitosamente el cupón: {}. En la siguiente liga podrás encontrar el código qr para escanear.

# {}
# """.format(name, coupon, url)

# def request_customer_email(name):
#     return """Gracias {}. Ahora porfavor, ingresa tu correo electrónico
# """.format(name)


# def hr_complete_guide(name):
#     return """{}, gracias por tu retroalimentación es muy valiosa para nosotros, ten por seguro que nos esforzaremos para que la siguiente semana sea mucho mejor
# """.format(name)


