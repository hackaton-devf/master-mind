### DELETE THIS FILE ONCE TELEGRAM_BOT IS REMOVED, it has already been moved under notify.py
from twilio.rest import Client

import os


def get_client():
    """Create an instance of our Bot"""
    return Client(os.getenv('TWILIO_SID'), os.getenv('TWILIO_AUTH_TOKEN'))


def send_message(client=None, **kwargs):
    exception = None
    client = client or get_client()

    try:
        message = client.messages.create(**kwargs)

    except Exception as e:
        print('Error al enviar', e)
        return e
