from django.contrib import admin
from django.db import models

from users.models import MasterMindUser

class MasterMindUserUserAdmin(admin.ModelAdmin):
    list_display = (
		'name',
		'surname',
		'email',
		'created',
    	)
    search_fields = ['name', 'email',]


admin.site.register(MasterMindUser, MasterMindUserUserAdmin)
