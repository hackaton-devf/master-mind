from django.db import models
# from django.core.files import File
# from django.template.loader import render_to_string
# from django.core.files.base import ContentFile
# from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from base.models import BaseModel
from helpers.files import upload_to_generic, upload_to_kwargs

USER_TYPE_CHOICES = (
    (1, 'No Type'),
    (2, 'Emprendedor'),
    (3, 'Empleado'),
    (4, 'Profesionista'),
    (5, 'Freelance'),
    (6, 'Estudiante'),
)

USER_TYPE_LEVEL = (
    (0, 'No Level'),
    (1, 'Avanzado'),
    (2, 'Intermedio'),
    (3, 'Básico'),
)


class MasterMindAbstractBaseUser(BaseModel):
    name = models.CharField(max_length=150, default='', blank=True)
    surname = models.CharField(max_length=150, default='', blank=True)
    email = models.EmailField(max_length=50, unique=True)
    whatsapp_id = models.CharField(max_length=150, default='', blank=True)
    area = models.CharField(max_length=150, default='', blank=True)
    level = models.PositiveIntegerField(default=0, choices=USER_TYPE_LEVEL, blank=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    photo = models.ImageField(upload_to=upload_to_kwargs(upload_to_generic, subfolder="user-photo"), null=True, blank=True)

    type = models.PositiveIntegerField(default=0, choices=USER_TYPE_CHOICES, blank=True)

    # objects = AssetsControlUserManager()

    USERNAME_FIELD = 'whatsapp_id'
    REQUIRED_FIELDS = ['name']

    class Meta:
        # This model should not be created in the database
        abstract = True

    def __str__(self):
        return "{email}".format(email=self.email)

    @property
    def is_staff(self):
        """Returns if the user can access to Django Admin site"""
        return self.is_admin

    @property
    def short_name(self):
        return self.name

    @property
    def get_short_name(self):
        return self.name

    @property
    def full_name(self):
        return "{} {}".format(self.name, self.surname)
