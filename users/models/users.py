from django.db import models
# from django.contrib.contenttypes.fields import GenericRelation

from users.models import MasterMindAbstractBaseUser
from base.models import BaseModel

# import binascii
# import os


class MasterMindUser(MasterMindAbstractBaseUser):

    def __str__(self):
        return self.full_name

    @property
    def is_staff(self):
        """Staff users always can use Django Admin Site"""
        return True
