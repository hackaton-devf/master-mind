# Generated by Django 3.0.7 on 2020-06-07 15:52

from django.db import migrations, models
import helpers.files


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='masterminduser',
            name='name',
            field=models.CharField(blank=True, default='', max_length=150),
        ),
        migrations.AlterField(
            model_name='masterminduser',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to=helpers.files.upload_to_generic),
        ),
    ]
